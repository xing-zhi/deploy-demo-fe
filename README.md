# 安装fis3
官方文档给出的是全局安装，我更倾向于在项目中安装，相关的操作通过在`package.json`文件中设置相关的脚本，然后是使用npm run运行。

# 目录结构

    ├── dist
    │   ├── css
    │   ├── imgs
    │   ├── index.html
    │   └── js
    ├── fis-conf.js
    ├── package.json
    ├── README.md
    └── src
        ├── css
        ├── imgs
        ├── index.html
        └── js

